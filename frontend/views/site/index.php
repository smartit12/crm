<?php
use kartik\date\DatePicker;
/* @var $this yii\web\View */

$this->title = 'CRM';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations! <?=(!empty($data['user']))?$data['user']->username:''?>  You have logged in successfully</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
           <?php

			// usage without model
			echo '<label>Check Issue Date</label>';
			echo DatePicker::widget([
			    'name' => 'check_issue_date', 
			    'value' => date('d-M-Y', strtotime('+2 days')),
			    'options' => ['placeholder' => 'Select issue date ...'],
			    'pluginOptions' => [
			        'format' => 'dd-M-yyyy',
			        'todayHighlight' => true
			    ]
			]); ?>
        </div>

    </div>
</div>
